import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { LoginComponent } from './login/login.component';
//import { RouterModule } from '@angular/router';
import { AdminRoutingModule } from './admin.route';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { AdminNavbarComponent } from './admin-navbar/admin-navbar.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AdminBooksComponent } from './admin-books/admin-books.component';


@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    CollapseModule.forRoot(),
  ],
  declarations: [LoginComponent, AdminDashboardComponent, AdminHeaderComponent, AdminNavbarComponent, AdminHomeComponent, AdminBooksComponent]
})
export class AdminModule { }
